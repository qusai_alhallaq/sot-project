import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:street_of_thing/helpers/enums.dart';
import 'package:street_of_thing/models/http_exception.dart';
import 'package:street_of_thing/widgets/gender_picker.dart';
import '../providers/auth.dart';
import '../mini_widgets/signup_card.dart';
import '../screens/terms_screen.dart';
import '../screens/verfication_screen.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();
  bool _obscurePassword = true;
  bool _obscureConfirm = true;
  bool _isTermAgree = false;
  bool _isLoading = false;
  bool _autoValidate = false;
  final _emailFocusNode = FocusNode();
  final _passwordFocusNode = FocusNode();
  final _confirmFocusNode = FocusNode();
  final _phoneFocusNode = FocusNode();
  var _crossAxisAlignment = CrossAxisAlignment.end;
  Color _autoValidateErorrColor = Colors.orange;
  RegExp _emailRegix = new RegExp(
    r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$",
    caseSensitive: false,
    multiLine: false,
  );
  RegExp _passwordUppercaseRegix = new RegExp(
    r'^(?=.*?[A-Z])',
    multiLine: false,
  );
  RegExp _passwordLowerCaseRegix = new RegExp(
    r'^(?=.*?[a-z])',
    multiLine: false,
  );
  RegExp _passwordNumberRegix = new RegExp(
    r'^(?=.*?[0-9])',
    multiLine: false,
  );
  RegExp _passwordSpetacCharRegix = new RegExp(
    r'^(?=.*?[!@#\$&*~])',
    multiLine: false,
  );
  RegExp _phoneNumberRegix = new RegExp(
    r'^(?:[+0]9)?[0-9]{10}',
    multiLine: false,
  );
  Map<String, String> _userSignupData = {
    'email': '',
    'Username': '',
    'password': '',
    'PasswordConfirmation': '',
    'PhoneNumber': '',
    'Gender': 'Male'
  };
  void _showDialog(String title, String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: Text('OK'))
        ],
      ),
    );
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      setState(() {
        _crossAxisAlignment = CrossAxisAlignment.center;
        _autoValidateErorrColor = Colors.redAccent[900];
      });
      // Invalid!
      return;
    }
    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      await Provider.of<Auth>(context, listen: false).signup(
          _userSignupData['email'],
          _userSignupData['Username'],
          _userSignupData['password'],
          _userSignupData['PasswordConfirmation'],
          _userSignupData['Gender'],
          _userSignupData['PhoneNumber']);
      // _showDialog('Sucsessfully ', 'sign up complete');
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (ctx) => VerficationScreen(_userSignupData['email']),
        ),
      );
    } on HttpException catch (error) {
      _showDialog('An error Occurred', error.toString());
    } catch (error) {
      print(error.toString());
      _showDialog('An error Occurred', error.toString());
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _passwordFocusNode.dispose();
    _confirmFocusNode.dispose();
    _phoneFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: _crossAxisAlignment,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 10, left: 50),
                child: Icon(
                  Icons.person_outline,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'User Name',
                    contentPadding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    labelStyle: const TextStyle(fontSize: 15),
                  ),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'this faield is required';
                    }
                    return null;
                  },
                  onSaved: (val) {
                    _userSignupData['Username'] = val;
                  },
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_emailFocusNode);
                  },
                ),
              ),
              const SizedBox(
                width: 50,
              )
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: _crossAxisAlignment,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 10),
                child: Icon(
                  Icons.mail_outline,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Email',
                    contentPadding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    labelStyle: const TextStyle(fontSize: 15),
                  ),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'this field is required';
                    }
                    if (!_emailRegix.hasMatch(val)) {
                      return 'invalid email';
                    }
                    return null;
                  },
                  onSaved: (val) {
                    _userSignupData['email'] = val;
                  },
                  focusNode: _emailFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_passwordFocusNode);
                  },
                ),
              ),
              const SizedBox(
                width: 50,
              )
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            crossAxisAlignment: _crossAxisAlignment,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 10),
                child: Icon(
                  _obscurePassword ? Icons.lock_outline : Icons.lock_open,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Expanded(
                child: TextFormField(
                  obscureText: _obscurePassword,
                  decoration: InputDecoration(
                    labelText: 'Password',
                    // errorStyle: TextStyle(color: _autoValidateErorrColor),
                    contentPadding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    labelStyle: const TextStyle(fontSize: 15),
                  ),
                  onSaved: (val) {
                    _userSignupData['password'] = val;
                  },
                  validator: (val) {
                    if (!_passwordLowerCaseRegix.hasMatch(val)) {
                      return 'at lest contain 1 LowerCase char';
                    } else if (!_passwordUppercaseRegix.hasMatch(val)) {
                      return 'at lest contain 1 UperCase char';
                    } else if (!_passwordNumberRegix.hasMatch(val)) {
                      return 'at lest contain one number';
                    } else if (!_passwordSpetacCharRegix.hasMatch(val)) {
                      return 'at lest contain one Symbole';
                    } else if (val.length < 6) {
                      return 'length must be at least 6 character';
                    } else
                      return null;
                  },
                  autovalidate: _autoValidate,
                  onTap: () {
                    setState(() {
                      _autoValidate = true;
                    });
                  },
                  focusNode: _passwordFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_confirmFocusNode);
                  },
                ),
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(_obscurePassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye),
                onPressed: () {
                  setState(() {
                    _obscurePassword = !_obscurePassword;
                  });
                },
                iconSize: 18,
              ),
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: _crossAxisAlignment,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 10),
                child: Icon(
                  _obscureConfirm ? Icons.lock_outline : Icons.lock_open,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Expanded(
                child: TextFormField(
                  obscureText: _obscureConfirm,
                  decoration: InputDecoration(
                    labelText: 'Confrim Password',
                    contentPadding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    labelStyle: const TextStyle(fontSize: 15),
                  ),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'this field is required';
                    }
                    return null;
                  },
                  onSaved: (val) {
                    _userSignupData['PasswordConfirmation'] = val;
                  },
                  focusNode: _confirmFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_phoneFocusNode);
                  },
                ),
              ),
              IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(_obscureConfirm
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye),
                onPressed: () {
                  setState(() {
                    _obscureConfirm = !_obscureConfirm;
                  });
                },
                iconSize: 18,
              ),
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: _crossAxisAlignment,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 10),
                child: Icon(
                  Icons.phone_android,
                  size: 25,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: 'Phone Number',
                    contentPadding:
                        const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    labelStyle: TextStyle(fontSize: 15),
                  ),
                  keyboardType: TextInputType.phone,
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'this field is required';
                    }
                    if (!_phoneNumberRegix.hasMatch(val)) {
                      return 'invalide phone number';
                    }
                    return null;
                  },
                  onSaved: (val) {
                    _userSignupData['PhoneNumber'] = val;
                  },
                  focusNode: _phoneFocusNode,
                ),
              ),
              const SizedBox(
                width: 50,
              )
            ],
          ),
          const SizedBox(
            height: 15,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: GenderPickerWithImage(
                  verticalAlignedText: true,
                  unSelectedGenderTextStyle: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                  onChanged: (gender) {
                    if (gender == Gender.Female) {
                      _userSignupData['Gender'] = 'Female';
                    } else {
                      _userSignupData['Gender'] = 'Male';
                    }
                  },
                  equallyAligned: true,
                  animationDuration: const Duration(milliseconds: 300),
                  isCircular: true,
                  opacityOfGradient: 0.4,
                  padding: const EdgeInsets.all(3),
                  size: 50, //default : 40
                ),
              ),
              // SizedBox(width: 75,height: 100,)
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Checkbox(
                    value: _isTermAgree,
                    activeColor: Theme.of(context).primaryColor,
                    checkColor: const Color(0xFF00efc8),
                    onChanged: (newVal) {
                      setState(() {
                        _isTermAgree = !_isTermAgree;
                      });
                    }),
                Text(
                  'I Agree to ',
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                InkWell(
                  child: Text(
                    'Terms',
                    style: TextStyle(
                      color: Color(0xFF33b5a7),
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => TermsScreen()));
                  },
                )
              ],
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          AnimatedOpacity(
            opacity: _isTermAgree ? 1.0 : 0.0,
            duration: const Duration(milliseconds: 1000),
            child: Container(
              height: 100,
              width: 200,
              alignment: Alignment.center,
              child: _isLoading
                  ? Center(child: CircularProgressIndicator())
                  : FlatButton(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 75, vertical: 15),
                      textColor: const Color(0xFF00efc8),
                      color: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          width: _isTermAgree ? 0 : 3,
                          color: Colors.grey[300],
                        ),
                        borderRadius: BorderRadius.circular(
                          30.0,
                        ),
                      ),
                      onPressed: _isTermAgree ? _submit : null,
                      child: FittedBox(
                        child: const Text(
                          'Sign Up',
                        ),
                      ),
                    ),
            ),
          ),
          const SizedBox(
            height: 30,
          )
        ],
      ),
    );
  }
}
