import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:street_of_thing/screens/tap_screen.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../screens/map_screen.dart';
import '../helpers/location_helper.dart';

class EditLocation extends StatefulWidget {
  @override
  _EditLocationState createState() => _EditLocationState();
}

class _EditLocationState extends State<EditLocation> {
  String _previewImageUrl;
  double _latitude, _longtude;
  void _showpreviewImage(double lat, double lng) {
    final staticMapImageUrl = LocationHelper.generateLocationPreviewImage(
      latitude: lat,
      longtude: lng,
    );
    setState(() {
      _previewImageUrl = staticMapImageUrl;
      _latitude = lat;
      _longtude = lng;
    });
  }

  Future<void> _getCurrentUserLocation() async {
    try {
      final loctionDate = await Location().getLocation();
      _showpreviewImage(loctionDate.latitude, loctionDate.longitude);
    } catch (err) {
      print(err.toString());
    }
  }

  Future<void> _selectOnMap() async {
    final selectedLocation = await Navigator.of(context).push<LatLng>(
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (ctx) => MapScreen(
          isSelecting: true,
        ),
      ),
    );
    if (selectedLocation == null) {
      print('error');
      return;
    }
    _showpreviewImage(selectedLocation.latitude, selectedLocation.longitude);
  }

  Widget _typeLocationButton(
      {IconData icon,
      String label,
      Function function,
      BoxConstraints constraints}) {
    return FlatButton.icon(
      // padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.03),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      color: Theme.of(context).accentColor,
      onPressed: function,
      icon: FittedBox(
        child: Icon(
          icon,
          color: Theme.of(context).primaryColor,
        ),
      ),
      splashColor: Colors.white,
      label: FittedBox(
        child: Text(
          label,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }

  Widget _portraitMode(BoxConstraints constraints) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: 0),
          height: constraints.maxHeight * 0.6,
          width: double.infinity,
          child: _previewImageUrl == null
              ? Center(
                  child: Text(
                    'No Location Chosen Yet !',
                    style: TextStyle(color: Theme.of(context).accentColor),
                    softWrap: true,
                  ),
                )
              : Stack(
                  children: <Widget>[
                    Center(child: const CircularProgressIndicator()),
                    Center(
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: _previewImageUrl,
                        fit: BoxFit.fill,
                        width: double.infinity,
                      ),
                    ),
                  ],
                ),
        ),
        SizedBox(
          height: constraints.maxHeight * 0.05,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.03),
          child: FittedBox(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _typeLocationButton(
                  function: _getCurrentUserLocation,
                  label: 'New Current Location',
                  icon: Icons.location_on,
                  constraints: constraints,
                ),
                SizedBox(
                  width: constraints.maxWidth * 0.03,
                ),
                _typeLocationButton(
                  function: _selectOnMap,
                  label: 'Choose on Map',
                  icon: Icons.map,
                  constraints: constraints,
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: constraints.maxHeight * 0.05,
        ),
        AnimatedOpacity(
          opacity: _previewImageUrl != null ? 1.0 : 0.0,
          duration: const Duration(milliseconds: 1000),
          child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.3),
            child: FittedBox(
              child: FlatButton.icon(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                splashColor: Colors.white,
                onPressed: _previewImageUrl == null
                    ? null
                    : () {
                        Navigator.of(context).pop(LatLng(_latitude, _longtude));
                      },
                label: Text(
                  'Save new Location',
                  style: TextStyle(color: Theme.of(context).accentColor),
                ),
                icon: Icon(
                  Icons.save,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _landscapeMode(BoxConstraints constraints) {
    return Row(
      children: <Widget>[
        Container(
          width: constraints.maxWidth * 0.70,
          height: constraints.maxHeight,
          child: _previewImageUrl == null
              ? Center(
                  child: Text(
                    'No Location Chosen Yet !',
                    style: TextStyle(color: Theme.of(context).accentColor),
                    softWrap: true,
                  ),
                )
              : Stack(
                  children: <Widget>[
                    Center(child: const CircularProgressIndicator()),
                    Center(
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: _previewImageUrl,
                        fit: BoxFit.fill,
                        width: constraints.maxWidth * 0.65,
                      ),
                    ),
                  ],
                ),
        ),
        Container(
          height: constraints.maxHeight,
          width: constraints.maxWidth * 0.25,
          padding: EdgeInsets.only(top: constraints.maxHeight * 0.05),
          margin: EdgeInsets.only(right: constraints.maxWidth * 0.01),
          child: FittedBox(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _typeLocationButton(
                    function: _getCurrentUserLocation,
                    label: 'New Current Location',
                    icon: Icons.location_on,
                    constraints: constraints),
                SizedBox(
                  height: constraints.maxHeight * 0.15,
                ),
                _typeLocationButton(
                    function: _selectOnMap,
                    label: 'Choose on Map',
                    icon: Icons.map,
                    constraints: constraints),
                SizedBox(
                  height: constraints.maxHeight * 0.15,
                ),
                AnimatedOpacity(
                  opacity: _previewImageUrl != null ? 1.0 : 0.0,
                  duration: const Duration(milliseconds: 1000),
                  child: FittedBox(
                    child: FlatButton.icon(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      splashColor: Colors.white,
                      onPressed: _previewImageUrl == null
                          ? null
                          : () {
                              Navigator.of(context)
                                  .pop(LatLng(_latitude, _longtude));
                            },
                      label: Text(
                        'Save new Location',
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      icon: Icon(
                        Icons.save,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final _isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return LayoutBuilder(
      builder: (ctx, constraints) {
        return _isPortrait
            ? _portraitMode(constraints)
            : _landscapeMode(constraints);
      },
    );
  }
}
