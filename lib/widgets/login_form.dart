import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/models/http_exception.dart';
import 'package:street_of_thing/screens/chose_location_screen.dart';
import '../providers/auth.dart';
import '../helpers/login_info.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formLoginKey = GlobalKey<FormState>();

  final _passwordFocusNode = FocusNode();

  final _whiteColor = Colors.white;
  bool _obscurePassword = true;
  var _crossAxisAlignment = CrossAxisAlignment.end;

  final RegExp _emailRegix = new RegExp(
    r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$",
    caseSensitive: false,
    multiLine: false,
  );

  Map<String, String> _userLoginData = {
    'email': '',
    'password': '',
  };
  bool _isLoading = false;
  double _latitude, _longtude;
  String _address;
  String _platformImie;

  void _showDialog(String title, String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: Text('OK'))
        ],
      ),
    );
  }

  Future<void> _submit() async {
    if (!_formLoginKey.currentState.validate()) {
      if (!mounted) {
        return;
      }
      setState(() {
        _crossAxisAlignment = CrossAxisAlignment.center;
      });
      return;
    }
    _formLoginKey.currentState.save();
    if (!mounted) {
      return;
    }
    setState(() {
      _isLoading = true;
    });
    try {
      await Provider.of<Auth>(context, listen: false)
          .checkLogin(_userLoginData['email'], _userLoginData['password']);

      // Navigator.of(context).pushReplacementNamed('/');
    } on HttpException catch (error) {
      _showDialog('An error Occurred', error.toString());
      if (!mounted) {
        return;
      }
      setState(() {
        _isLoading = false;
      });
      return;
    } catch (error) {
      _showDialog('An error Occurred', 'Something wrong ... Please try later');
      if (!mounted) {
        return;
      }
      setState(() {
        _isLoading = false;
      });
      return;
    }
    try {
      await Provider.of<Auth>(context, listen: false).login(
          _userLoginData['email'],
          _userLoginData['password'],
          'Lon=$_longtude&Lat=$_latitude&Imei=$_platformImie');
    } on HttpException catch (error) {
      _showDialog('An error Occurred ', error.toString());
      if (!mounted) {
        return;
      }
      setState(() {
        _isLoading = false;
      });
      return;
    } catch (error) {
      _showDialog('An error Occurred ', 'Please chose your location');
      if (!mounted) {
        return;
      }
      setState(() {
        _isLoading = false;
      });
      return;
    }
    if (!mounted) {
      return;
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void dispose() {
    _passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    return Form(
      key: _formLoginKey,
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: _crossAxisAlignment,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 10),
                child: Icon(
                  Icons.mail_outline,
                  size: 25,
                  color: _whiteColor,
                ),
              ),
              Expanded(
                child: TextFormField(
                  style: TextStyle(color: _whiteColor),
                  decoration: InputDecoration(
                    labelText: 'Email',
                    labelStyle: TextStyle(fontSize: 15, color: _whiteColor),
                    counterStyle: TextStyle(fontSize: 15, color: _whiteColor),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: _whiteColor),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: _whiteColor),
                    ),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  ),
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'this field is required';
                    }
                    if (!_emailRegix.hasMatch(val)) {
                      return 'invalid email';
                    }
                    return null;
                  },
                  onSaved: (val) {
                    _userLoginData['email'] = val;
                  },
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_passwordFocusNode);
                  },
                ),
              ),
              SizedBox(
                width: 50,
              ),
            ],
          ),
          SizedBox(
            height: mediaQueryData.size.height * 0.02,
          ),
          Row(
            crossAxisAlignment: _crossAxisAlignment,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 10),
                child: Icon(
                  _obscurePassword ? Icons.lock_outline : Icons.lock_open,
                  size: 25,
                  color: _whiteColor,
                ),
              ),
              Expanded(
                child: TextFormField(
                  style: TextStyle(color: _whiteColor),
                  decoration: InputDecoration(
                    labelText: 'Password',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: _whiteColor),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: _whiteColor),
                    ),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                    labelStyle: TextStyle(fontSize: 15, color: _whiteColor),
                  ),
                  obscureText: _obscurePassword,
                  validator: (val) {
                    if (val.isEmpty) {
                      return 'this field is required';
                    }
                    return null;
                  },
                  onSaved: (val) {
                    _userLoginData['password'] = val;
                  },
                  onFieldSubmitted: (_) {
                    _submit();
                  },
                  focusNode: _passwordFocusNode,
                ),
              ),
              IconButton(
                padding: const EdgeInsets.only(right: 5),
                color: _whiteColor,
                icon: Icon(_obscurePassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye),
                onPressed: () {
                  setState(() {
                    _obscurePassword = !_obscurePassword;
                  });
                },
                iconSize: 18,
              ),
            ],
          ),
          SizedBox(
            height: mediaQueryData.size.height * 0.035,
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50, right: 10),
                child: Icon(
                  Icons.person_pin_circle,
                  // FontAwesomeIcons.mapPin
                  size: 25,
                  color: _whiteColor,
                ),
              ),
              Expanded(
                child: GestureDetector(
                    onTap: () async {
                      final data = await Navigator.of(context).push<LoginInfo>(
                        MaterialPageRoute(
                          builder: (ctx) => ChoseLocationScreen(),
                        ),
                      );
                      // print(data.lat);
                      if (!mounted) {
                        return;
                      }
                      setState(() {
                        _latitude = data.lat;
                        _longtude = data.lng;
                        _platformImie = data.imime;
                        _address = data.address;
                      });
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          _address == null ? '  Choose Your Location' : '  $_address',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: Theme.of(context).textTheme.body1.fontFamily
                          ),
                        ),
                        // SizedBox(height: mediaQueryData.size.height*0.01,),
                        Divider(
                          color: Colors.white,
                          thickness: 1,
                        ),
                      ],
                    )),
              ),
              SizedBox(
                width: 50,
              ),
            ],
          ),
          SizedBox(
            height: mediaQueryData.size.height * 0.035,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: _isLoading
                ? const CircularProgressIndicator()
                : FlatButton(
                    padding: EdgeInsets.symmetric(
                        horizontal: mediaQueryData.size.width * 0.28),
                    onPressed: _submit,
                    color: const Color(0xFF33b5a7),
                    //24a4e4    33b5a7
                    child: Text(
                      'LOG IN',
                      style: TextStyle(color: _whiteColor),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
