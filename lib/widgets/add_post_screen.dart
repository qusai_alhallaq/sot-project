import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/models/http_exception.dart';
import 'package:street_of_thing/providers/single_post.dart';
import '../providers/posts.dart';

enum PostType {
  Free,
  Lending,
}

class AddPostScreen extends StatefulWidget {
  static const String routName = '/add-post-screen';
  @override
  _AddPostScreenState createState() => _AddPostScreenState();
}

class _AddPostScreenState extends State<AddPostScreen> {
  final _formEditPostKey = GlobalKey<FormState>();
  final _discriptionFocusNode = FocusNode();
  PostType _postType = PostType.Free;
  List<Asset> images = List<Asset>();
  List<Map<String, String>> _tagsList = List();
  List<String> _tags = [];
  List<int> _selectedIndexList = List();
  bool _selectionMode = false;
  bool _isLoading = false;
  Map<String, Object> _postData = {
    'objectName': '',
    'Description': '',
  };
  void _showDialog(String title, String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: Text('OK'))
        ],
      ),
    );
  }

  Future<void> _submit() async {
    if (!_formEditPostKey.currentState.validate()) {
      return;
    }
    if (_tags.isEmpty) {
      _showDialog(
          'something wrong', 'plase select at least tow tags to your item');
      return;
    }

    if (images.isEmpty) {
      _showDialog(
          'something wrong', 'plase select at least one photo for item');
      return;
    }

    _formEditPostKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      await Provider.of<Posts>(context, listen: false).addPost(
          new SinglePost(
              id: null,
              title: _postData['objectName'],
              description: _postData['Description'],
              images: null,
              tags: _tags,
              type: _postType.toString().split('.').last),
          images);
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pop();
    } on HttpException catch (error) {
      _showDialog('error', error.toString());
      setState(() {
        _isLoading = false;
      });
    } catch (error) {
      _showDialog('error', 'something get wrong');
      setState(() {
        _isLoading = false;
      });
    }
  }

  Widget buildGridView() {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 3,
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
    });
  }

  @override
  void initState() {
    super.initState();

    _tagsList.add({'tag': 'Manual', 'url': 'assets/images/paint.png'});
    _tagsList.add({'tag': 'Electrec', 'url': 'assets/images/wood.png'});
    _tagsList.add({'tag': 'tool', 'url': 'assets/images/paint.png'});
    _tagsList.add({'tag': 'home', 'url': 'assets/images/wood.png'});
    _tagsList.add({'tag': 'keachen', 'url': 'assets/images/paint.png'});
    _tagsList.add({'tag': 'hand', 'url': 'assets/images/wood.png'});
  }

  void _changeSelection({bool enable, int index}) {
    _selectionMode = enable;
    _selectedIndexList.add(index);
    if (index == -1) {
      _selectedIndexList.clear();
    }
  }

  Widget _createBody() {
    return StaggeredGridView.countBuilder(
      shrinkWrap: true,
      crossAxisCount: 3,
      mainAxisSpacing: 30.0,
      crossAxisSpacing: 30.0,
      primary: false,
      itemCount: _tagsList.length,
      itemBuilder: (BuildContext context, int index) {
        return getGridTile(index);
      },
      staggeredTileBuilder: (int index) => StaggeredTile.count(1, 1),
      padding: const EdgeInsets.all(4.0),
    );
  }

  GridTile getGridTile(int index) {
    if (_selectionMode) {
      return GridTile(
          header: GridTileBar(
            leading: Icon(
              _selectedIndexList.contains(index)
                  ? Icons.check_circle_outline
                  : Icons.radio_button_unchecked,
              color: _selectedIndexList.contains(index)
                  ? Theme.of(context).accentColor
                  : Colors.black,
            ),
          ),
          child: GestureDetector(
            child: Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  border: Border.all(
                      color: Theme.of(context).accentColor, width: 4.0),
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Theme.of(context).primaryColor),
              child: Image.asset(
                _tagsList[index]['url'],
                fit: BoxFit.cover,
              ),
            ),
            onLongPress: () {
              setState(() {
                _changeSelection(enable: false, index: -1);
              });
            },
            onTap: () {
              setState(() {
                if (_selectedIndexList.contains(index)) {
                  _selectedIndexList.remove(index);
                  _tags.remove(_tagsList[index]['tag']);
                } else {
                  _selectedIndexList.add(index);
                  _tags.add(_tagsList[index]['tag']);
                }
              });
            },
          ));
    } else {
      return GridTile(
        child: InkResponse(
          child: Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                border: Border.all(color: Theme.of(context).primaryColor),
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Theme.of(context).primaryColor),
            // color: Theme.of(context).primaryColor,
            child: Image.asset(
              _tagsList[index]['url'],
              fit: BoxFit.cover,
            ),
          ),
          onTap: () {
            setState(() {
              _changeSelection(enable: true, index: index);
              _tags.add(_tagsList[index]['tag']);
            });
          },
        ),
      );
    }
  }

  @override
  void dispose() {
    _discriptionFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Upload Item'),
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Form(
              key: _formEditPostKey,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: mediaQueryData.size.width * 0.06),
                child: ListView(
                  children: <Widget>[
                    InkWell(
                      onTap: _submit,
                      child: Container(
                        margin: EdgeInsets.only(top: 40, bottom: 30),
                        height: mediaQueryData.size.height * 0.1,
                        width: mediaQueryData.size.width * 0.15,
                        child: Image.asset(
                          'assets/images/upload-solid.png',
                          // fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: TextFormField(
                              decoration: InputDecoration(
                                labelText: "Enter Item Name",
                                labelStyle: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'this field is required';
                                }
                                return null;
                              },
                              onSaved: (val) {
                                _postData['objectName'] = val;
                              },
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(_discriptionFocusNode);
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: TextFormField(
                              // style: TextStyle(fontSize: 18),
                              maxLines: 3,
                              decoration: InputDecoration(
                                labelText: "Item Description",
                                labelStyle: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              validator: (val) {
                                if (val.isEmpty) {
                                  return 'this field is required';
                                }
                                return null;
                              },
                              onSaved: (val) {
                                _postData['Description'] = val;
                              },
                              focusNode: _discriptionFocusNode,
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                        'Item Type',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RadioListTile<PostType>(
                              title: const Text('Free'),
                              value: PostType.Free,
                              groupValue: _postType,
                              onChanged: (PostType value) {
                                setState(() {
                                  _postType = value;
                                  print(_postType.toString().split('.').last);
                                });
                              },
                            ),
                          ),
                          Expanded(
                            child: RadioListTile<PostType>(
                              title: const Text('Lending'),
                              value: PostType.Lending,
                              groupValue: _postType,
                              onChanged: (PostType value) {
                                setState(() {
                                  _postType = value;
                                  print(_postType.toString().split('.').last);
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text('Select Tags',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Theme.of(context).primaryColor)),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: _createBody(),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Item Image',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                          FlatButton.icon(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                color: Colors.grey[300],
                              ),
                              borderRadius: BorderRadius.circular(
                                15.0,
                              ),
                            ),
                            color: Theme.of(context).primaryColor,
                            icon: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                            label: Text(
                              "add",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            onPressed: loadAssets,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: buildGridView(),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
