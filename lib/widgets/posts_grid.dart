import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/widgets/post_information.dart';
import '../providers/posts.dart';

class PostsGrid extends StatelessWidget { 
  @override
  Widget build(BuildContext context) {
    final postsData = Provider.of<Posts>(context);
    final posts = postsData.posts;
    return ListView.builder(
      itemCount: posts.length,
      // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
      //   crossAxisCount: 1,
      //   mainAxisSpacing: 10, 
      // ),
      itemBuilder: (ctx, i) => 
      ChangeNotifierProvider.value(
        value: posts[i],
        child: PostInformation(),
      ),
    );
  }
}
