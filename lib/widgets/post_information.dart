import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/providers/single_post.dart';
import 'package:street_of_thing/screens/comments_screen.dart';
import 'package:street_of_thing/screens/post_detail_screen.dart';

class PostInformation extends StatelessWidget {
  @override 
  Widget build(BuildContext  context) {
    final SinglePost postData = Provider.of<SinglePost>(context, listen: false);
    return Consumer<SinglePost>(
        builder: (ctx, p, _) => InkWell(
              onTap: () {
                Navigator.of(context).pushNamed(PostDetailsScreen.routeName,
                    arguments: postData.id);
              },
              child: Card(
                color: Theme.of(context).primaryColor,
                elevation: 4,
                margin: EdgeInsets.only(top: 10, right: 10, left: 10),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8),
                      child: ListTile(
                        leading: CircleAvatar(
                          // radius: 30,
                          backgroundImage: NetworkImage(postData.owner.image
                              .replaceAll('localhost', '192.168.43.169')),
                        ),
                        title: Text(
                          postData.owner.name,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.3,
                      child: Image.network(
                        postData.images[0]
                            .toString()
                            .replaceAll('localhost', '192.168.43.169'),
                        fit: BoxFit.cover,
                        width: double.infinity,
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(top: 8, right: 8, left: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '${postData.title}',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          RatingBarIndicator(
                            rating: 2,
                            itemBuilder: (context, index) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            itemCount: 5,
                            itemSize: 25.0,
                            unratedColor: Colors.amber.withAlpha(50),
                            direction: Axis.horizontal,
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8, left: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FittedBox(
                            child: Text(
                              'Houerly Rent : ${postData.price} S.P',
                              style: TextStyle(
                                  color: Theme.of(context).accentColor),
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.comment,
                              color: Theme.of(context).accentColor,
                            ),
                            onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (ctx) => CommentsScreen(objectId: postData.id,),
                              ),
                              
                            ),
                          ),
                          Consumer<SinglePost>(
                            builder: (ctx, postD, _) => IconButton(
                              padding: EdgeInsets.only(right: 0),
                              icon: Icon(
                                postD.isFavorait
                                    ? Icons.favorite
                                    : Icons.favorite_border,
                                color: Theme.of(context).accentColor,
                              ),
                              onPressed: () {
                                postD.toggleFavorite();
                                // Scaffold.of(context).showSnackBar(SnackBar(content: Text('Data')));
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 1,
                    )
                    // GridTile(
                    //   child: Image.network(
                    //     postData.images,
                    //     fit: BoxFit.cover,
                    //   ),
                    //   footer: GridTileBar(
                    //     backgroundColor: Colors.black54,
                    //     leading: FittedBox(
                    //       child: Text('${postData.price} per hour'),
                    //     ),
                    //     title: FittedBox(child: Text(postData.title)),
                    //     trailing: RatingBarIndicator(
                    //       rating: 2,
                    //       itemBuilder: (context, index) => Icon(
                    //         Icons.star,
                    //         color: Colors.amber,
                    //       ),
                    //       itemCount: 5,
                    //       itemSize: 50.0,
                    //       unratedColor: Colors.amber.withAlpha(50),
                    //       direction: Axis.horizontal,
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
            )
        // ClipRRect(
        //   borderRadius: BorderRadius.circular(10),
        //   child: Column(
        //     children: <Widget>[
        //       ListTile(leading: CircleAvatar(backgroundColor: Colors.red),
        //       title: Text('User Name'),),
        //       GridTile(
        //         child: GestureDetector(
        //           child: Image.network(postData.images,
        //           fit: BoxFit.cover,),
        //         ),
        //         footer: GridTileBar(
        //           backgroundColor: Colors.black54,
        //           leading: FittedBox(
        //             child: Text('${postData.price} per hour'),
        //           ),
        //           title: FittedBox(child: Text(postData.title)),
        //           trailing: RatingBarIndicator(
        //             rating:2,
        //             itemBuilder: (context, index) => Icon(
        //               Icons.star,
        //               color: Colors.amber,
        //             ),
        //             itemCount: 5,
        //             itemSize: 50.0,
        //             unratedColor: Colors.amber.withAlpha(50),
        //             direction: Axis.horizontal,
        //           ),
        //         ),
        //       ),
        //     ],
        //   ),
        // );
        );
  }
}
