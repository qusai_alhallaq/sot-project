import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/providers/comment.dart';
import 'package:street_of_thing/widgets/read_more.dart';
class SingleComment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    Comment comment  = Provider.of<Comment>(context,listen: false);
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
          width: mediaQueryData.size.width * 0.75,
          child: ListTile(
            contentPadding: EdgeInsets.all(8),
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
            ),
            title: Transform(
              transform: Matrix4.translationValues(-8, 0.0, 0.0),
              child: Card(
                color: Theme.of(context).accentColor,
                elevation: 4,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white70, width: 1),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'user name',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      ReadMoreText(
                        comment.text,
                        trimLines: 3,
                        colorClickableText: Colors.white,
                        trimMode: TrimMode.Line,
                        trimCollapsedText: '...Show more',
                        trimExpandedText: ' show less',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
// class CommentsListKeyPrefix {
//   static final String singleComment = "Comment";
//   static final String commentUser = "Comment User";
//   static final String commentText = "Comment Text";
//   static final String commentDivider = "Comment Divider";
// }

// final List<String> comments = ['asdsd', 'adsasd'];

// class CommentsList extends StatelessWidget {
//   const CommentsList({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('data'),
//       ),
//       body: Padding(
//           padding: const EdgeInsets.only(top: 8.0),
//           child: ListView.builder(
//             itemCount:    comments.length,
//             itemBuilder: (ctx, int index) => _SingleComment(
//               index: index,
//             ),
//           )),
//     );
//   }
// }

// class _SingleComment extends StatelessWidget {
//   final int index;

//   const _SingleComment({Key key, @required this.index}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final String commentData = comments[index];

//     return Container(
//       width: double.infinity,
//       margin: const EdgeInsets.symmetric(vertical: 8.0),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           // UserDetailsWithFollow(
//           //   key: ValueKey("${CommentsListKeyPrefix.commentUser} $index"),
//           //   userData: commentData.user,
//           // ),
//           Text(
//             commentData,
//             textAlign: TextAlign.left,
//           ),
//           // Divider(
//           //   color: Colors.black45,
//           // ),
//         ],
//       ),
//     );
//   }
// }
