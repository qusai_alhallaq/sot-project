import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:location/location.dart';
import 'package:street_of_thing/helpers/login_info.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../screens/map_screen.dart';
import '../helpers/location_helper.dart';

class ChoseLocationAndGetImei extends StatefulWidget {
  @override
  _ChoseLocationAndGetImeiState createState() =>
      _ChoseLocationAndGetImeiState();
}

class _ChoseLocationAndGetImeiState extends State<ChoseLocationAndGetImei> {
  String _platformImei = 'Unknown';
  String _previewImageUrl;
  double _latitude;
  double _longtude;
  bool _isloading = false;
  void _showpreviewImage(double lat, double lng) {
    final staticMapImageUrl = LocationHelper.generateLocationPreviewImage(
      latitude: lat,
      longtude: lng,
    );
    setState(() {
      _latitude = lat;
      _longtude = lng;
      _previewImageUrl = staticMapImageUrl;
    });
  }

  Future<void> _getCurrentUserLocation() async {
    try {
      final loctionDate = await Location().getLocation();
      _showpreviewImage(loctionDate.latitude, loctionDate.longitude);
    } catch (err) {
      print(err.toString());
    }
  }

  Future<void> _selectOnMap() async {
    final selectedLocation = await Navigator.of(context).push<LatLng>(
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (ctx) => MapScreen(
          isSelecting: true,
        ),
      ),
    );
    if (selectedLocation == null) {
      print('error');
      return;
    }
    _showpreviewImage(selectedLocation.latitude, selectedLocation.longitude);
  }

  // Future<void> _getAddress() async {
  //   final add = await LocationHelper.getPlaceAddress(
  //       latitude: _latitude, longtude: _longtude);
  //   if (!mounted) {
  //     return;
  //   }
  //   setState(() {
  //     _address = add;
  //   });
  // }

  Future<void> _saveToServer() async {
    if (!mounted) {
      return;
    }
    setState(() {
      _isloading = true;
    });
    final address = await LocationHelper.getPlaceAddress(
        latitude: _latitude, longtude: _longtude);

    if (!mounted) {
      return;
    }
    setState(() {
      _isloading = false;
    });
    print(address);
    Navigator.of(context)
        .pop(LoginInfo(_latitude, _longtude, _platformImei, address));
    print(_platformImei);
    setState(() {
      _isloading = true;
    });
  }

  Widget _typeLocationButton(
      {IconData icon,
      String label,
      Function function,
      BoxConstraints constraints}) {
    return FlatButton.icon(
      // padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.03),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      color: Theme.of(context).accentColor,
      onPressed: function,
      icon: FittedBox(
        child: Icon(
          icon,
          color: Theme.of(context).primaryColor,
        ),
      ),
      splashColor: Colors.white,
      label: FittedBox(
        child: Text(
          label,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformImei;
    try {
      platformImei =
          await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: false);
    } on PlatformException {
      platformImei = 'Failed to get platform version.';
    }
    if (!mounted) return;

    setState(() {
      _platformImei = platformImei;
    });
  }

  Widget _portraitMode(BoxConstraints constraints) {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(
              // vertical: _isPortrait
              //     ? 0//constraints.maxHeight * 0.02
              //     : constraints.maxHeight * 0.05,
              horizontal: 0),
          height: constraints.maxHeight * 0.6,

          width: double.infinity,
          // decoration: BoxDecoration(
          //     image: _previewImageUrl != null
          //         ? DecorationImage(
          //             image: NetworkImage(_previewImageUrl),
          //             fit: BoxFit.fill,
          //           )
          //         : null,
          //     borderRadius: BorderRadius.circular(15),
          //     border: Border.all(color: Colors.grey, width: 1)),
          child: _previewImageUrl == null
              ? Center(
                  child: Text(
                    'No Location Chosen Yet !',
                    style: TextStyle(color: Theme.of(context).accentColor),
                    softWrap: true,
                  ),
                )
              : Stack(
                  children: <Widget>[
                    Center(child: const CircularProgressIndicator()),
                    Center(
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: _previewImageUrl,
                        fit: BoxFit.fill,
                        width: double.infinity,
                      ),
                    ),
                  ],
                ),
        ),
        SizedBox(
          height: constraints.maxHeight * 0.05,
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.03),
          child: FittedBox(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _typeLocationButton(
                  function: _getCurrentUserLocation,
                  label: 'Current Location',
                  icon: Icons.location_on,
                  constraints: constraints,
                ),
                SizedBox(
                  width: constraints.maxWidth * 0.03,
                ),
                _typeLocationButton(
                  function: _selectOnMap,
                  label: 'Choose on Map',
                  icon: Icons.map,
                  constraints: constraints,
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: constraints.maxHeight * 0.05,
        ),
        AnimatedOpacity(
          opacity: _previewImageUrl != null ? 1.0 : 0.0,
          duration: const Duration(milliseconds: 1000),
          child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.3),
            child: _isloading
                ? Center(child: CircularProgressIndicator())
                : FittedBox(
                    child: FlatButton.icon(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      splashColor: Colors.white,
                      onPressed: _previewImageUrl == null
                          ? null
                          : () {
                              _saveToServer();

                              // Navigator.of(context).pushReplacementNamed('/');
                            },
                      label: Text(
                        'Save Location',
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      icon: Icon(
                        Icons.save,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  ),
          ),
        ),
      ],
    );
  }

  Widget _landscapeMode(BoxConstraints constraints) {
    return Row(
      children: <Widget>[
        Container(
          width: constraints.maxWidth * 0.70,
          height: constraints.maxHeight,
          child: _previewImageUrl == null
              ? Center(
                  child: Text(
                    'No Location Chosen Yet !',
                    style: TextStyle(color: Theme.of(context).accentColor),
                    softWrap: true,
                  ),
                )
              : Stack(
                  children: <Widget>[
                    Center(child: const CircularProgressIndicator()),
                    Center(
                      child: FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: _previewImageUrl,
                        fit: BoxFit.fill,
                        width: constraints.maxWidth * 0.65,
                      ),
                    ),
                  ],
                ),
        ),
        Container(
          height: constraints.maxHeight,
          width: constraints.maxWidth * 0.25,
          padding: EdgeInsets.only(top: constraints.maxHeight * 0.05),
          margin: EdgeInsets.only(right: constraints.maxWidth * 0.01),
          child: FittedBox(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                _typeLocationButton(
                    function: _getCurrentUserLocation,
                    label: 'Current Location',
                    icon: Icons.location_on,
                    constraints: constraints),
                SizedBox(
                  height: constraints.maxHeight * 0.15,
                ),
                _typeLocationButton(
                    function: _selectOnMap,
                    label: 'Choose on Map',
                    icon: Icons.map,
                    constraints: constraints),
                SizedBox(
                  height: constraints.maxHeight * 0.15,
                ),
                AnimatedOpacity(
                  opacity: _previewImageUrl != null ? 1.0 : 0.0,
                  duration: const Duration(milliseconds: 1000),
                  child: _isloading
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : FittedBox(
                          child: FlatButton.icon(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            splashColor: Colors.white,
                            onPressed: _previewImageUrl == null
                                ? null
                                : () {
                                    _saveToServer();
                                    // Navigator.of(context)
                                    //     .pushReplacementNamed(TabeScreen.routeName);
                                  },
                            label: Text(
                              'Save Location',
                              style: TextStyle(
                                  color: Theme.of(context).accentColor),
                            ),
                            icon: Icon(
                              Icons.save,
                              color: Theme.of(context).accentColor,
                            ),
                          ),
                        ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final _isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    return LayoutBuilder(
      builder: (ctx, constraints) {
        return _isPortrait
            ? _portraitMode(constraints)
            : _landscapeMode(constraints);
      },
    );
  }
}
