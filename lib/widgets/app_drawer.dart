import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/auth.dart';
class AppDrawer extends StatelessWidget {
  Widget listTileBuilder(IconData leading, String title, Function onTap) {
    return ListTile(
      leading: Icon(leading),
      title: Text(title),
      onTap: onTap,
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    final bool _isProtrait = mediaQueryData.orientation == Orientation.portrait;
    // print((mediaQueryData.size.width));
    return Drawer(
      elevation: 4,
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topLeft,
            children: <Widget>[
              Container(
                  // padding:
                  //     EdgeInsets.only(top: mediaQueryData.size.height * 0.1),
                  alignment: Alignment.bottomCenter,
                  height: _isProtrait
                      ? (mediaQueryData.size.height -
                              mediaQueryData.padding.top) *
                          0.30
                      : (mediaQueryData.size.height -
                              mediaQueryData.padding.top) *
                          0.6,
                  color: Theme.of(context).primaryColor,
                  child: Image.asset(
                    'assets/images/SOT.jpg',
                    fit: BoxFit.cover,
                  )),
              Padding(
                padding: EdgeInsets.only(
                    top: mediaQueryData.size.height * 0.038,
                    left: mediaQueryData.size.width * 0.034),
                child: Row(
                  children: <Widget>[
                    AvatarGlow(
                      startDelay: Duration(milliseconds: 1000),
                      glowColor: Colors.white,
                      endRadius: 45.0,
                      duration: Duration(milliseconds: 3000),
                      repeat: true,
                      showTwoGlows: true,
                      repeatPauseDuration: Duration(milliseconds: 100),
                      child: Material(
                        elevation: 8.0,
                        shape: CircleBorder(),
                        color: Colors.transparent,
                        child: CircleAvatar(
                          backgroundImage: AssetImage('assets/images/ver.jpg'),
                          radius: 30.0,
                        ),
                      ),
                      shape: BoxShape.circle,
                      animate: true,
                      curve: Curves.ease,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Qusai Abd',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: mediaQueryData.size.height * 0.005,
                        ),
                        Text('+96399839499',
                            style: TextStyle(
                              color: Colors.white54,
                            )),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
          Divider(),
          listTileBuilder(Icons.settings, 'Settings', () {}),
          listTileBuilder(Icons.chat, 'Invint Neighbors', () {}),
          listTileBuilder(Icons.stars, 'Rate Us', () {}),
          listTileBuilder(Icons.mail, 'Contact Us', () {}),
          listTileBuilder(Icons.power_settings_new, 'Log Out', () {
            Navigator.of(context).pop();
            // Navigator.of(context).pushReplacementNamed('/');
            Provider.of<Auth>(context,listen: false).logout();
          }),
        ],
      ),
    );
  }
}
