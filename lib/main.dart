import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/providers/comment.dart';
import 'package:street_of_thing/providers/posts.dart';
import 'package:street_of_thing/providers/profile.dart';
import 'package:street_of_thing/providers/single_post.dart';
import 'package:street_of_thing/screens/post_detail_screen.dart';
import 'package:street_of_thing/screens/splash_screen.dart';
import 'package:street_of_thing/screens/tap_screen.dart';
import 'package:street_of_thing/widgets/add_post_Screen.dart';
import 'package:street_of_thing/widgets/posts_grid.dart';
import './helpers/custom_route.dart';
import './providers/auth.dart';
import './screens/login_screen.dart';
import './screens/signup_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  static Map<int, Color> color = {
    50: Color.fromRGBO(14, 19, 38, .1),
    100: Color.fromRGBO(14, 19, 38, .2),
    200: Color.fromRGBO(14, 19, 38, .3),
    300: Color.fromRGBO(14, 19, 38, .4),
    400: Color.fromRGBO(14, 19, 38, .5),
    500: Color.fromRGBO(14, 19, 38, .6),
    600: Color.fromRGBO(14, 19, 38, .7),
    700: Color.fromRGBO(14, 19, 38, .8),
    800: Color.fromRGBO(14, 19, 38, .9),
    900: Color.fromRGBO(14, 19, 38, 1),
  };

  final MaterialColor colorCustom = MaterialColor(0xFF0e1326, color);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
        ChangeNotifierProxyProvider<Auth,Posts>(
          create: (_) => Posts(),
          update: (_, auth, previousProfile) {
            previousProfile.authntToken = auth.token;
            return previousProfile;
          },
        ), 
        ChangeNotifierProxyProvider<Auth,Comment>(
          create: (_) => Comment(),
          update: (_, auth, previousComment) {
            previousComment.authntToken = auth.token;
            return previousComment;
          },
        ), 
        ChangeNotifierProxyProvider<Auth, Profile>(
          create: (_) => Profile(),
          update: (_, auth, previousProfile) {
            previousProfile.authntToken = auth.token;
            return previousProfile;
          },
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
          title: 'Street Of things',
          theme: ThemeData( 
            textSelectionColor: const Color(0xFF33b5a7),
            textSelectionHandleColor: const Color(0xFF33b5a7),
            primarySwatch: colorCustom,
            accentColor: const Color(0xFF33b5a7),
            canvasColor: Color.fromRGBO(255, 255, 255, 1),
            fontFamily: 'seguisym',
            pageTransitionsTheme: PageTransitionsTheme(
              builders: {
                TargetPlatform.android: CustomPageTransitionBuilder(),
                TargetPlatform.iOS: CustomPageTransitionBuilder(),
              },
            ),
            textTheme: ThemeData.light().textTheme.copyWith(
                  body1: TextStyle(color: Color.fromRGBO(20, 51, 51, 1)),
                  body2: TextStyle(
                    color: Color.fromRGBO(20, 51, 51, 1),
                  ),
                  title: TextStyle(
                    fontSize: 20,
                    fontFamily: 'RobotoCondensed',
                    fontWeight: FontWeight.bold,
                  ),
                ),
          ),
          home: 
          // TabeScreen(),
          auth.isAuth
              ? TabeScreen()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, authDataSnapshot) =>
                      authDataSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : LoginScreen()),
          routes: {
            SignUpScreen.routName: (cxt) => SignUpScreen(),
            TabeScreen.routeName: (ctx) => TabeScreen(),
            AddPostScreen.routName: (ctx) => AddPostScreen(),
            PostDetailsScreen.routeName: (ctx) => PostDetailsScreen(),
          },
        ),
      ),
    );
  }
}
