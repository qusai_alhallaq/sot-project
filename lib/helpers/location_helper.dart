import 'dart:convert';
import 'package:http/http.dart' as http;

const GOOGLE_API_KEY = 'AIzaSyBOWTck0gkVqv4VPIkIOwR-JBMu9GzMklk';

class LocationHelper {
  static generateLocationPreviewImage({double latitude, double longtude}) {
    return 'https://maps.googleapis.com/maps/api/staticmap?center=&$latitude,$longtude&zoom=16&size=600x1200&maptype=roadmap&markers=color:red%7Clabel:C%7C$latitude,$longtude&key=$GOOGLE_API_KEY';
  }

  static Future<String> getPlaceAddress ({double latitude, double longtude})async{
    final url =
    'https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longtude&key=$GOOGLE_API_KEY';
    final response = await http.get(url);
    return json.decode(response.body)['results'][0]['formatted_address'];
  }
}
