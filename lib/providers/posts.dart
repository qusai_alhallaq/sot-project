import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:street_of_thing/models/http_exception.dart';
import 'package:street_of_thing/providers/single_post.dart';
import 'package:http/http.dart' as http;
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:http_parser/http_parser.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';

const String IP_ADDRESS = '192.168.43.169';

class Posts with ChangeNotifier {
  List<SinglePost> _posts = [
    // SinglePost(
    //   id: '1',
    //   title: 'Laptop',
    //   description:
    //       'Dell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GB Dell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GB Dell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GB Dell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GBDell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GB Dell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GBDell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GBDell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GBDell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GBDell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GBDell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GBDell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GB',
    //   images: [
    //     'assets/images/22.jpg',
    //     'assets/images/33.jpg',
    //     'assets/images/ver.jpg'
    //   ],
    //   // price: 10000,
    // ),
    // SinglePost(
    //   id: '2',
    //   title: 'laptop',
    //   description: 'Dell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GB',
    //   images: ['assets/images/33.jpg'],
    //   price: 5.5,
    // ),
    // SinglePost(
    //   id: '3',
    //   title: 'laptop',
    //   description: 'Dell Laptop with 8 Gb Ram ,hard hdd 500 GB and ssd 240 GB',
    //   images: ['assets/images/ver.jpg'],
    //   price: 5.5,
    // )
  ];
  List<SinglePost> get posts {
    return [..._posts];
  }

  SinglePost findById(String id) {
    return _posts.firstWhere((post) => post.id == id);
  }

  String _authToken;
  set authntToken(String token) {
    _authToken = token;
  }

  Future<void> addPost(SinglePost singlePost, List<Asset> asset) async {
    final url = 'http://$IP_ADDRESS:25000/api/catalog/object/create';

    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $_authToken',
      },
      body: json.encode(
        {
          'objectName': singlePost.title,
          'Description': singlePost.description,
          'Tags': singlePost.tags,
          'Type': singlePost.type,
        },
      ),
    );

    final responseData = json.decode(response.body);
    print(responseData);

    if (responseData['errorCode'] != null) {
      throw HttpException(responseData['message']);
    }
    await _uploadPostImages(asset, responseData['objectId']);
    // final newPost = SinglePost(
    //   id: responseData['objectId'],
    //   title: singlePost.title,
    //   description: singlePost.description,
    //   tags: singlePost.tags,
    //   type: singlePost.type,
    //   images: ['assets/images/male.png', 'assets/images/female.png'],
    // );
    // _posts.insert(0, newPost);
    notifyListeners();
  }

  Future<void> _uploadPostImages(List<Asset> asset, int objectId) async {
    Uri uri = Uri.parse(
        'http://$IP_ADDRESS:25000/api/catalog/object/$objectId/uploadphoto');
    for (Asset image in asset) {
      http.MultipartRequest request = http.MultipartRequest("POST", uri);
      request.headers.addAll({'Authorization': 'Bearer $_authToken'});
      var path = await FlutterAbsolutePath.getAbsolutePath(image.identifier);
      print(path);
      String fileName = path.split('/').last;
      print(fileName);
      var multipartFile = await http.MultipartFile.fromPath(
        'file',
        path,
        filename: fileName,
        contentType: MediaType("image", "jpg"),
      );

// add file to multipart
      request.files.add(multipartFile);
// send
      http.Response response =
          await http.Response.fromStream(await request.send());
      print(response.statusCode);
      final responseData = json.decode(response.body);
      if (responseData['errorCode'] != null) {
        throw HttpException(responseData['message']);
      }
    }
  }

  Future<void> fetchPosts() async {
    final url = 'http://$IP_ADDRESS:25000/api/catalog/object/list';
    List<SinglePost> loadedPost = [];
    final response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $_authToken',
      },
    );
    final responseData = json.decode(response.body);
    // print(responseData);
    for (var postData in responseData) {
      // print( SinglePost.fromJson(postData).owner.image);
      loadedPost.add(
        SinglePost.fromJson(postData),
        // SinglePost(
        //   id: postData['id'].toString(),
        //   title: postData['name'],
        //   description: postData['description'],
        //   rating: postData['rating'],
        //   images: postData['photos'],
        //   countOfImpressions: postData['countOfImpressions'],
        //   countOfViews: postData['countOfViews'],
        //   // price:
        //   tags: postData['tags'],
        //   type: postData['type'],
        //   owner: Profile.fromJson(postData['owner']),
        // ),
      );
    }
    _posts = loadedPost;
    notifyListeners();
    // responseData.forEach((postId, postData) {
    //   loadedPost.add(SinglePost(
    //     id: postId,
    //     title: postData['name'],
    //     description: postData['description'],
    //     rating: postData['rating'],
    //     images: postData['photos'],
    //     countOfImpressions: postData['countOfImpressions'],
    //     countOfViews : postData['countOfViews'],
    //     // price:
    //     tags: postData['tags'],
    //     type: postData['type'],
    //     owner: postData['owner'],
    //   ));
    // });
  }
}
