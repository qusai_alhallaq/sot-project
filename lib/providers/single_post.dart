import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:street_of_thing/providers/profile.dart';

// enum TypeOfObject{
// Free,
// Lending,
// }
class SinglePost with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final List<dynamic> tags;
  final String type;
  final List<dynamic> images;
  double price;
  final countOfViews;
  final countOfImpressions;
  double rating;
  final Profile owner;

  bool isFavorait;
  SinglePost({
    @required this.id,
    @required this.title,
    @required this.description,
    this.tags,
    this.type,
    @required this.images,
    this.price = 100,
    this.countOfViews,
    this.countOfImpressions,
    this.rating = 1,
    this.isFavorait = false,
    this.owner,
  });
  SinglePost.fromJson(Map<String, dynamic> json)
      : id = json['id'].toString(),
        title = json['name'],
        description = json['description'],
        tags = json['tags'],
        type = json['type'],
        images = json['photos'],
        price = 100,
        countOfViews = json['countOfViews'],
        countOfImpressions = json['countOfImpressions'],
        isFavorait = false,
        owner = Profile.fromJson(json['owner']);

  //  Map<String, dynamic> toJson() =>
  //   {
  //     'id': id,
  //     'name': title,
  //   };
  void setStateVal(bool oldVal) {
    isFavorait = oldVal;
    notifyListeners();
  }

  Future<void> toggleFavorite() {
    var oldStatuse = isFavorait;
    isFavorait = !isFavorait;
    notifyListeners();
  }
}
