import 'dart:io';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;
import 'package:street_of_thing/helpers/location_helper.dart';
import 'package:street_of_thing/providers/auth.dart';

class Profile with ChangeNotifier {
  final String id;
  final String userName;
  final String name;
  final String email;
  final String image;
  String phoneNumber;
  String gender;

  Profile({this.id, this.userName, this.image, this.email, this.name});
  Profile.fromJson(Map<String, dynamic> json)
      : id = json['id'] as String,
        name = json['name'] as String,
        userName = json['userName'] as String,
        email = json['email'] as String,
        image = json['pictureUrl'] as String,
        phoneNumber = json['phoneNumber'] as String,
        gender = json['gender'];

  String _authToken;
  set authntToken(String token) {
    _authToken = token;
  }

  Profile prof;
  Profile get profile {
    return prof;
  }

  Future<void> getProfileInformation() async {
    final url = 'http://$IP_ADDRESS:25000/api/identity/profile/whoami';
    final response = await http.get(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $_authToken',
      },
    );
    final responseData = json.decode(response.body);
    Profile profile = Profile.fromJson(responseData);
    prof = profile;
    // print(profile.gender);
    // print(profile.name);
    // print(profile.id);
    // print(profile.phoneNumber);
    notifyListeners();
  }

  Future<void> uploadImageProile(File file) async {
    final String url =
        "http://192.168.1.7:25000/api/identity/profile/uploadPhoto";
    Dio dio = new Dio();
    // dio.options.headers["authorization"] = "Bearer $_authToken";
    String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(file.path,
          filename: fileName, contentType: new MediaType("image", "jpeg")),
    });
    print('response');

    final response = await dio.post(
      url,
      data: formData,
      options: Options(
        headers: {"Authorization": 'Bearer $_authToken'},
      ),
    );
    // final response = await http.post(url, headers: {
    //   'Authorization': 'Bearer $_authToken'
    // }, body: base64Image);
    print(response);
    notifyListeners();
  }
}
