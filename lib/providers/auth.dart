import 'dart:convert';
import 'dart:async';
import 'package:flutter/widgets.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../models/http_exception.dart';
const String IP_ADDRESS = '192.168.43.169';
class Auth with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  // String _userId;
  Timer _authTimer;
  bool get isAuth {
    // print(_token!=null);
    return token != null;
  }

  String get token {
    if (_token != null &&
        _expiryDate != null &&
        _expiryDate.isAfter(
          DateTime.now(),
        )) {
      return _token;
    }
    return null;
  }

  Future<void> signup(
    String email,
    String userName,
    String password,
    String confirmPassword,
    String gender,
    String phoneNumber,
  ) async {
    const url = 'http://$IP_ADDRESS:25000/api/identity/account/register';
    final response = await http.post(
      url,
      headers: {"Content-Type": "application/json"},
      body: json.encode(
        {
          'email': email,
          'Username': userName,
          'password': password,
          'PasswordConfirmation': confirmPassword,
          'PhoneNumber': phoneNumber,
          'Gender': gender
        },
      ),
    );
    print(response.statusCode);
    final responseData = json.decode(response.body);
    print(responseData);
    if (responseData['errorCode'] != null) {
      throw HttpException(responseData['message']);
    }
    // return 'mhjh';
  }

  Future<void> verfication(String email, String code) async {
    const url = 'http://$IP_ADDRESS:25000/api/identity/account/confirm';
    final response = await http.post(
      url,
      headers: {"Content-Type": "application/json"},
      body: json.encode(
        {
          'email': email,
          'confirmationCode': code,
        },
      ),
    );
    final responseData = json.decode(response.body);
    if (responseData['errorCode'] != null) {
      throw HttpException(responseData['message']);
    }
  }

  Future<void> resendVerficationCode(String email) async {
    const url = 'http://$IP_ADDRESS:25000/api/identity/account/ResendCode';
    final response = await http.post(
      url,
      headers: {"Content-Type": "application/json"},
      body: json.encode(
        {
          'email': email,
        },
      ),
    );
    final responseData = json.decode(response.body);
    if (responseData['errorCode'] != null) {
      throw HttpException(responseData['message']);
    }
  }

  Future<void> checkLogin(String email, String password) async {
    const url = 'http://$IP_ADDRESS:25000/api/identity/account/check';
    final response = await http.post(url,
        headers: {'content-type': 'application/json'},
        body: json.encode({
          'Username': email,
          'Password': password,
        }));
    final resposeData = json.decode(response.body);
    if (resposeData['errorCode'] != null) {
      throw HttpException(resposeData['message']);
    }
  }

  Future<void> login(String email, String password, String loginInfo) async {
    const url = 'http://$IP_ADDRESS:25000/identity/connect/token';
    try {
      final response = await http.post(url,
          headers: {'content-type': 'application/json'},
          body: json.encode({
            'username': email,
            'password': password,
            "grant_type": "password",
            'loginInfo': loginInfo,
          }));
      final responseData = json.decode(response.body);
      if (responseData['errorCode'] != null) {
        throw HttpException(responseData['message']);
      }
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: responseData['expires_in'],
        ),
      );
      _token = responseData['access_token'];
      _autoLogout();
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      final userAuthData = json.encode(
          {'token': _token, 'expiryDate': _expiryDate.toIso8601String()});
      prefs.setString('userAuthData', userAuthData);
      // print(_token);
    } catch (error) {
      throw error;
    }
  }

  Future<void> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userAuthData')) {
      return false;
    }
    final extractedUserAuthData =
        json.decode(prefs.getString('userAuthData')) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserAuthData['expiryDate']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserAuthData['token'];
    _expiryDate = expiryDate;
    notifyListeners();
    _autoLogout();
    return true;
  }

  Future<void> logout() async {
    _token = null;
    // print(_token);
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    // prefs.remove('userAuthData');
    prefs.clear();
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    final timeExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeExpiry), logout);
  }
}
