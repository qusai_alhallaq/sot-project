import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:street_of_thing/models/http_exception.dart';

const String IP_ADDRESS = '192.168.43.169';

class Comment with ChangeNotifier {
  final String text;
  Comment({
    this.text,
  });

  String _authToken;
  set authntToken(String token) {
    _authToken = token;
  }

  List<Comment> _comments = [];

  List<Comment> get comments {
    return [..._comments];
  }

  Future<void> uploadComment(String comment, int objectId) async {
    final url = 'http://$IP_ADDRESS:25000/api/catalog/object/comment/add';
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $_authToken',
      },
      body: json.encode(
        {
          'objectId': objectId,
          'comment': comment,
        },
      ),
    );
    // print(response.statusCode);
    final responseData = json.decode(response.body);
    if(responseData['errorCode'] != null){
      throw HttpException(responseData['Message']);
    }
    _comments.add(Comment(text: comment));
    notifyListeners();
  }
}
