import 'package:flutter/material.dart';


class SingupCard extends StatelessWidget {
  const SingupCard();
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(0),
      elevation: 5,
      child: Container(
        width: double.infinity,
        child: Image.asset(
          'assets/images/signup.jpg',
          fit: BoxFit.cover,
          width: double.infinity,
        ),
      ),
    );
  }
}
