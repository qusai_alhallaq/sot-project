import 'package:flutter/cupertino.dart';

class UserLocation {
  final double longtude;
  final double latitude;
  final String address;
  const UserLocation({
    @required this.longtude,
    @required this.latitude,
    this.address,
  });
}
