import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:street_of_thing/screens/filter_screen.dart';
import 'package:street_of_thing/screens/home_screen.dart';
import 'package:street_of_thing/screens/notification_screen.dart';
import 'package:street_of_thing/screens/profile_screen.dart';
import 'package:street_of_thing/screens/trasaction_screen.dart';
import 'package:street_of_thing/widgets/add_post_Screen.dart';
import '../widgets/app_drawer.dart';

class TabeScreen extends StatefulWidget {
  static const String routeName = '/tab-screen';

  @override
  _TabeScreenState createState() => _TabeScreenState();
}

class _TabeScreenState extends State<TabeScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 2;
  @override
  void initState() {
    _pages = [
      {'page': FilterScreen(), 'title': 'Filters'},
      {'page': ProfileScreen(), 'title': 'My Profile'},
      {'page': HomeScreen(), 'title': 'Home'},
      {'page': NotificationScrenn(), 'title': 'Notification'},
      {'page': TransactionScreen(), 'title': 'Transactions'},
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _mediaQueryData = MediaQuery.of(context);
    var appBar2 = AppBar(
      title: Text(_pages[_selectedPageIndex]['title']),
      actions: <Widget>[
        IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.white,
            ),
            onPressed: () =>
                Navigator.of(context).pushNamed(AddPostScreen.routName))
      ],
    );
    return Scaffold(
      appBar: appBar2,
      drawer: AppDrawer(),
      body: _pages[_selectedPageIndex]['page'],
      bottomNavigationBar: CurvedNavigationBar(
        index: 2,
        height: appBar2.preferredSize.height,
        items: <Widget>[
          Icon(
            Icons.filter_list,
            size: 25,
            color: Theme.of(context).accentColor,
          ),
          Icon(
            Icons.person,
            size: 25,
            color: Theme.of(context).accentColor,
          ),
          Icon(
            Icons.home,
            size: 25,
            color: Theme.of(context).accentColor,
          ),
          Icon(
            Icons.notifications,
            size: 25,
            color: Theme.of(context).accentColor,
          ),
          Icon(
            // Icons.import_export,
            FontAwesomeIcons.handshake,
            size: 25,
            color: Theme.of(context).accentColor,
          ),
        ],
        color: Theme.of(context).primaryColor,
        buttonBackgroundColor: Theme.of(context).primaryColor,
        backgroundColor: Colors.white70,
        animationCurve: Curves.easeInOut,
        animationDuration: Duration(milliseconds: 400),
        onTap: _selectPage,
      ),
      //  BottomNavigationBar(
      //   onTap: _selectPage,
      //   backgroundColor: Theme.of(context).primaryColor,
      //   // selectedItemColor:,

      //   items: [
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.filter_list),
      //       title: Text('Filter'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.person),
      //       title: Text('profile'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.home),
      //       title: Text('home'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.notifications),
      //       title: Text('notification'),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.transform),
      //       title: Text('transactions'),
      //     ),
      //   ],
      // ),
      //  BottomAppBar(
      //   // shape: CircularNotchedRectangle(),
      //   color: Theme.of(context).primaryColor,
      //   child: Container(
      //     height: 75,
      //     child: Row(
      //       mainAxisSize: MainAxisSize.max,
      //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //       children: <Widget>[
      //         IconButton(
      //           iconSize: 30.0,
      //           padding: EdgeInsets.only(left: 28.0),
      //           icon: Icon(Icons.home),
      //           onPressed: () {},
      //           color: Theme.of(context).accentColor,
      //         ),
      //         IconButton(
      //           iconSize: 30.0,
      //           padding: EdgeInsets.only(right: 28.0),
      //           icon: Icon(Icons.search),
      //           onPressed: () {},
      //           color: Theme.of(context).accentColor,
      //         ),
      //         IconButton(
      //           iconSize: 30.0,
      //           padding: EdgeInsets.only(left: 28.0),
      //           icon: Icon(Icons.notifications),
      //           onPressed: () {},
      //           color: Theme.of(context).accentColor,
      //         ),
      //         IconButton(
      //           iconSize: 30.0,
      //           padding: EdgeInsets.only(right: 28.0),
      //           icon: Icon(Icons.list),
      //           onPressed: () {},
      //           color: Theme.of(context).accentColor,
      //         )
      //       ],
      //     ),
      //   ),
      // ),
      // floatingActionButton: Container(
      //   child: FittedBox(
      //     child: FloatingActionButton(
      //       elevation: 4,
      //       backgroundColor: Theme.of(context).accentColor,
      //       onPressed: null,
      //       child: Icon(
      //         FontAwesomeIcons.home,
      //         color: Theme.of(context).primaryColor,
      //       ),
      //     ),
      //   ),
      // ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
