import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/widgets/posts_grid.dart';
import '../providers/posts.dart';

class HomeScreen extends StatefulWidget {
  // static List<Asset> images = [] ;
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var _isInit = true;
  var _isLoading = false;
  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<Posts>(context).fetchPosts().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
          _isLoading ? Center(child: CircularProgressIndicator()) : PostsGrid(),
    );
  }
}
