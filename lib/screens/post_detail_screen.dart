import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:street_of_thing/widgets/read_more.dart';
import '../providers/posts.dart';

class PostDetailsScreen extends StatelessWidget {
  static const routeName = '/post-details-screen';
  @override
  Widget build(BuildContext context) {
    String postId = ModalRoute.of(context).settings.arguments as String;
    final loadedPost =
        Provider.of<Posts>(context, listen: false).findById(postId);

    final List<Widget> imageSliders = loadedPost.images
        .map((item) => Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Image.network(
                      item.toString().replaceAll('localhost', '192.168.43.169'),
                      fit: BoxFit.fill,
                      width: double.infinity)),
            ))
        .toList();

    return Scaffold(
      appBar: AppBar(
        title: Text(loadedPost.title),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: ListView(
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (ctx) => FullscreenSlider(imageSliders),
                ),
              );
            },
            child: NoonLooping(imageSliders),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 15),
            child: Text(
              'Detailes',
              style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: Theme.of(context).textTheme.title.fontSize),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
            child: ReadMoreText(
              loadedPost.description,
              trimLines: 3,
              colorClickableText: Theme.of(context).accentColor,
              trimMode: TrimMode.Line,
              trimCollapsedText: '...Show more',
              trimExpandedText: ' show less',
              style: TextStyle(color: Colors.white),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FlatButton.icon(
                  onPressed: null,
                  icon: Icon(
                    Icons.location_on,
                    color: Theme.of(context).accentColor,
                  ),
                  label: Text(
                    'View on Map',
                    style: TextStyle(color: Theme.of(context).accentColor),
                  )),
              FlatButton.icon(
                  onPressed: () { 
                    // AwesomeDialog(
                    //   context: context,
                    //   animType: AnimType.SCALE,
                    //   dialogType: DialogType.INFO,
                    //   body: Center(
                    //     child: Text(
                    //       'If the body is specified, then title and description will be ignored, this allows to further customize the dialogue.',
                    //       style: TextStyle(fontStyle: FontStyle.italic),
                    //     ),
                    //   ),
                    //   title: 'This is Ignored',
                    //   desc: 'This is also Ignored',
                    // )..show();
                  },
                  icon: Icon(
                    Icons.sentiment_satisfied,
                    color: Theme.of(context).accentColor,
                  ),
                  label: Text(
                    'Reservation',
                    style: TextStyle(color: Theme.of(context).accentColor),
                  )),
            ],
          )
        ],
      ),
    );
  }
}

class NoonLooping extends StatelessWidget {
  final List<Widget> imageSliders;
  NoonLooping(this.imageSliders);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CarouselSlider(
        options: CarouselOptions(
          aspectRatio: 1,
          enlargeCenterPage: true,
          enableInfiniteScroll: false,
          initialPage: 2,
          autoPlay: true,
        ),
        items: imageSliders,
      ),
    );
  }
}

class FullscreenSlider extends StatelessWidget {
  final List<Widget> imageSliders;
  FullscreenSlider(this.imageSliders);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Builder(
        builder: (context) {
          final double height = MediaQuery.of(context).size.height;
          return CarouselSlider(
              options: CarouselOptions(
                height: height,
                viewportFraction: 1.0,
                enlargeCenterPage: false,
                // autoPlay: false,
              ),
              items: imageSliders);
        },
      ),
    );
  }
}
