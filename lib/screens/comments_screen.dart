import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/models/http_exception.dart';
import 'package:street_of_thing/widgets/single_comment.dart';
import '../providers/comment.dart';

class CommentsScreen extends StatefulWidget {
  final String objectId;
  CommentsScreen({this.objectId});
  @override
  _CommentsScreenState createState() => _CommentsScreenState();
}

class _CommentsScreenState extends State<CommentsScreen> {
  final commentController = TextEditingController();
  bool _isLoading = false;

  void _showDialog(String title, String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: Text('OK'))
        ],
      ),
    );
  }

  Future<void> _submitComment() async {
    setState(() {
      _isLoading = true;
    });
    try {
      await Provider.of<Comment>(context, listen: false)
          .uploadComment(commentController.text, int.parse(widget.objectId));
    } on HttpException catch (error) {
      _showDialog('error', error.toString());
    } catch (error) {
      _showDialog('error', 'something get wrong');
    } finally {
      setState(() {
        _isLoading = false;
        commentController.text ='';
      });
    }
  }

  @override
  void dispose() {
    commentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final comments = Provider.of<Comment>(context).comments;
    final mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Reviews'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: comments.length,
              itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
                value: comments[i],
                child: SingleComment(),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: mediaQueryData.size.height * 0.007,
              horizontal: mediaQueryData.size.width * 0.01,
            ),
            child: TextField(
              controller: commentController,
              cursorColor: Theme.of(context).accentColor,
              style: TextStyle(color: Colors.white),
              decoration: new InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(
                    Icons.send,
                    color: Theme.of(context).accentColor,
                  ),
                  onPressed: () {
                    print('send');
                    _submitComment();
                  },
                ),
                enabledBorder: new OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).accentColor, width: 3),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
                focusedBorder: new OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Theme.of(context).accentColor,
                    width: 3,
                  ),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(30.0),
                  ),
                ),
                // filled: true,
                hintStyle: new TextStyle(color: Colors.grey[600]),
                hintText: "Type in your Review",
                // fillColor: Theme.of(context).accentColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
