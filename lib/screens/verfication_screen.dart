import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/models/http_exception.dart';
import 'package:street_of_thing/providers/auth.dart';
import 'package:street_of_thing/screens/login_screen.dart';
import '../external_packages/verification_widget.dart';

class VerficationScreen extends StatefulWidget {
  final String email;
  VerficationScreen(this.email);

  @override
  _VerficationScreenState createState() => _VerficationScreenState();
}

class _VerficationScreenState extends State<VerficationScreen> {
  bool _isResending = false;
  bool _isTimerFinshed = false;
  int _timerMunites = 1;
  void _showDialog(String title, String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                Navigator.of(ctx).pop();
              },
              child: Text('OK'))
        ],
      ),
    );
  }

  Future<void> _submitCode(String code, BuildContext context) async {
    try {
      await Provider.of<Auth>(context, listen: false)
          .verfication(widget.email, code);
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (ctx) => LoginScreen()));
    } on HttpException catch (error) {
      _showDialog('An error Occurred', error.toString());
    }
  }

  Future<void> resendCode(BuildContext ctx) async {
    try {
      setState(() {
        _isResending = true;
      });
      await Provider.of<Auth>(ctx,listen: false).resendVerficationCode(widget.email);
      setState(() {
        _timerMunites = 15;
      });
      _showDialog('Resending Code Successfully', 'Please check you inbox mail');
    } on HttpException catch (error) {
      _showDialog('An error Occurred', error.toString());
    }
    setState(() {
      _isResending = false;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      title: Text('Verfication'),
    );
    final mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(mediaQueryData.size.height * 0.017),
              height: mediaQueryData.size.height * 0.35,
              child:
                  Image.asset('assets/images/ver.jpg', width: double.infinity),
            ),
            SizedBox(
              height: mediaQueryData.size.height * 0.028,
            ),
            Text(
              "Please Enter The Code",
              softWrap: true,
              style: Theme.of(context).textTheme.title,
            ),
            SizedBox(
              height: mediaQueryData.size.height * 0.028,
            ),
            FittedBox(
              child: VerificationCode(
                length: 6,
                onCompleted: (code) {
                  _submitCode(code, context);
                },
                onEditing: (val) {},
                textStyle: TextStyle(
                    color: Color(0xFF33b5a7),
                    fontSize: Theme.of(context).textTheme.title.fontSize),
              ),
            ),
            SizedBox(
              height: mediaQueryData.size.height * 0.028,
            ),
            InkWell(
              customBorder: CircleBorder(),
              splashColor: Theme.of(context).accentColor,
              child: Container(
                height: mediaQueryData.size.height * 0.0975,
                width: mediaQueryData.size.width * 0.178,
                child: Image.asset('assets/images/complete.png'),
              ),
              onTap: () {},
            ),
            SizedBox(
              height: mediaQueryData.size.height * 0.028,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text('If not received during : '),
                CountdownFormatted(
                  onFinish: () {
                    setState(() {
                      _isTimerFinshed = true;
                    });
                  },
                  duration: Duration(minutes: _timerMunites),
                  builder: (BuildContext ctx, String remaining) {
                    return Text(remaining); // 01:00:00
                  },
                ),
                InkWell(
                  onTap: _isTimerFinshed ? () {resendCode(context);
                  setState(() {
                    _isTimerFinshed = false;
                  });}  : null,
                  child: _isResending
                      ? CircularProgressIndicator()
                      : Text(
                          'Resend',
                          style: TextStyle(
                              color: Theme.of(context).accentColor),
                        ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
