import 'dart:ui';

import 'package:flutter/material.dart';
import './signup_screen.dart';
import '../widgets/login_form.dart';

class LoginScreen extends StatelessWidget {
  static const String routeName = '/homme-sign-screen';
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    final appBar = AppBar(
        // title: Text(''),
        );
    print(mediaQueryData.size.height -
        appBar.preferredSize.height -
        mediaQueryData.padding.top);
        print(mediaQueryData.size.width);
    return Scaffold(
      appBar: appBar,
      backgroundColor: Theme.of(context).primaryColor,
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 40, bottom: 30),
                  height: mediaQueryData.size.height * 0.181,
                  width: mediaQueryData.size.width * 0.331,
                  child: Image.asset(
                    'assets/images/lock.png',
                    // fit: BoxFit.cover,
                  ),
                ),
                LoginForm(),
                SizedBox(
                  height: mediaQueryData.size.height * 0.15,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: FittedBox(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    child: Text(
                      'Sign Up',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color(0xFF33b5a7),
                        fontSize: 14,
                        // decoration: TextDecoration.underline,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).pushNamed(SignUpScreen.routName);
                    },
                  ),
                  Text(
                    ' for Street of Things',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      // decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
