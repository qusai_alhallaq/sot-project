import 'package:flutter/material.dart';
import 'package:street_of_thing/widgets/edit_location.dart';

class EditLocationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Location'),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: EditLocation(),
    );
  }
}
