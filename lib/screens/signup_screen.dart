import 'dart:async';
import 'package:flutter/material.dart';
import '../mini_widgets/signup_card.dart';
import '../widgets/signup_form.dart';

class SignUpScreen extends StatelessWidget {
  static const String routName = '/singup-form';
  

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    final appBar = AppBar(
      title: const Text('Sign up'),
    );
    final bool _isPortrait = mediaQueryData.orientation == Orientation.portrait;
    return Scaffold(
      appBar: appBar,
      body: ListView(
        children: <Widget>[
          _isPortrait
              ? Container(
                  height: mediaQueryData.size.height * 0.3,
                  child: const SingupCard())
              : const SizedBox(),
          const SizedBox(
            height: 15,
          ),
          SignUpForm(),
        ],
      ),
    );
  }
}
