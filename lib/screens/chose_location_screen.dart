import 'package:flutter/material.dart';
import '../widgets/chose_location_and_get_imei.dart';

class ChoseLocationScreen extends StatelessWidget {
  // final String email;
  // final String password;
  // ChoseLocationScreen(this.email, this.password);
  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      title: Text('Choose Your Location'),
    );
    final mediaQueryData = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: appBar,
      body: Container(
          height: mediaQueryData.size.height -
              appBar.preferredSize.height -
              mediaQueryData.padding.top,
          child: ChoseLocationAndGetImei()),
    );
  }
}
