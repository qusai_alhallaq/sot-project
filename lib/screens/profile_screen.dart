import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:street_of_thing/screens/edit_location_screen.dart';
import '../helpers/location_helper.dart';
import '../providers/profile.dart';
enum SourceImage { Camera, Gallery }

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  static File _image;
  static String _userName = 'Qusai Abd';
  static String _phoneNumber = '+963998390499';
  static double _latitude = 34.7;
  static double _longtude = 36.7;
  static String _address = '';
  SourceImage _sourceImage;
  final _userNameController = TextEditingController(text: _userName);
  final _phoneNumberController = TextEditingController(text: _phoneNumber);
  Future<void> _getImage() async {
    var image = await ImagePicker.pickImage(
        source: _sourceImage == SourceImage.Gallery
            ? ImageSource.gallery
            : ImageSource.camera);
    if (image == null) {
      return;
    }
    
    
    if (!mounted) {
      return;
    }
    setState(() {
      _image = image;
    });
    Provider.of<Profile>(context,listen: false).uploadImageProile(image);
  }

  Future<void> _getAddress() async {
    final add = await LocationHelper.getPlaceAddress(
        latitude: _latitude, longtude: _longtude);
    if (!mounted) {
      return;
    }
    setState(() {
      _address = add;
    });
  }

  void _choseImageSource(BuildContext ctx) {
    showModalBottomSheet(
        backgroundColor: Theme.of(context).primaryColor,
        context: ctx,
        builder: (_) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FlatButton.icon(
                onPressed: () {
                  if (!mounted) {
                    return;
                  }
                  setState(() {
                    _sourceImage = SourceImage.Camera;
                  });
                  _getImage();
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.camera_alt,
                  color: Theme.of(context).accentColor,
                ),
                label: Text(
                  'Camera',
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
              FlatButton.icon(
                onPressed: () {
                  if (!mounted) {
                    return;
                  }
                  setState(() {
                    _sourceImage = SourceImage.Gallery;
                  });
                  _getImage();
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.photo_library,
                  color: Theme.of(context).accentColor,
                ),
                label: Text(
                  'gallery',
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ],
          );
        });
  }

  void _editUserName(
    BuildContext ctx,
  ) {
    showModalBottomSheet(
      backgroundColor: Theme.of(context).primaryColor,
      context: ctx,
      builder: (context) => Container(
        padding: EdgeInsets.only(
          top: 10,
          right: 10,
          left: 10,
          bottom: MediaQuery.of(context).viewInsets.bottom + 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'Enter your name',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: Theme.of(context).textTheme.title.fontSize),
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            TextFormField(
              controller: _userNameController,
              style: TextStyle(
                color: Colors.white,
              ),
              maxLength: 50,
              // initialValue: _userName,
              decoration: InputDecoration(
                labelStyle: TextStyle(
                    fontSize: 15, color: Theme.of(context).accentColor),
                counterStyle: TextStyle(fontSize: 15, color: Colors.grey),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Theme.of(context).accentColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Theme.of(context).accentColor),
                ),
                // hintText: _userName
              ),
              cursorColor: Theme.of(context).accentColor,
              textCapitalization: TextCapitalization.words,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('cancel',
                      style: TextStyle(color: Theme.of(context).accentColor)),
                ),
                FlatButton(
                    onPressed: () {
                      if (!mounted) {
                        return;
                      }
                      setState(() {
                        _userName = _userNameController.text;
                      });
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'save',
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }

  void _editPhoneNumber(
    BuildContext ctx,
  ) {
    showModalBottomSheet(
      backgroundColor: Theme.of(context).primaryColor,
      context: ctx,
      builder: (context) => Container(
        padding: EdgeInsets.only(
          top: 10,
          right: 10,
          left: 10,
          bottom: MediaQuery.of(context).viewInsets.bottom + 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'Enter the new phone number ',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: Theme.of(context).textTheme.title.fontSize),
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.01,
            ),
            TextFormField(
              controller: _phoneNumberController,
              style: TextStyle(
                color: Colors.white,
              ),
              maxLength: 13,
              // initialValue: _userName,
              decoration: InputDecoration(
                labelStyle: TextStyle(
                    fontSize: 15, color: Theme.of(context).accentColor),
                counterStyle: TextStyle(fontSize: 15, color: Colors.grey),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Theme.of(context).accentColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Theme.of(context).accentColor),
                ),
              ),
              keyboardType: TextInputType.number,
              cursorColor: Theme.of(context).accentColor,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('cancel',
                      style: TextStyle(color: Theme.of(context).accentColor)),
                ),
                FlatButton(
                    onPressed: () {
                      if (!mounted) {
                        return;
                      }
                      setState(() {
                        _phoneNumber = _phoneNumberController.text;
                      });
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'save',
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _listTileBuilder(BuildContext ctx, IconData leadingIcon, String title,
      String subtitle, Function onTapFun) {
    return ListTile(
      leading: Padding(
          padding: EdgeInsets.only(bottom: 10),
          child: Icon(
            leadingIcon,
            size: 32,
          )),
      title: Text(title),
      subtitle: Text(
        subtitle,
        style: TextStyle(fontSize: 12),
      ),
      trailing: Icon(
        Icons.edit,
        size: 22,
      ),
      onTap: () => onTapFun(ctx),
    );
  }

  @override
  Widget build(BuildContext context) {
    
    _getAddress();
    final mediaQuery = MediaQuery.of(context);
    final _isPortrait = mediaQuery.orientation == Orientation.portrait;
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Padding(
            padding:
                EdgeInsets.symmetric(vertical: mediaQuery.size.height * 0.03),
            child: Center(
              child: Stack(
                children: <Widget>[
                  GestureDetector(
                    onTap: () =>
                        _image == null ? _choseImageSource(context) : null,
                    child: CircleAvatar(
                      radius: _isPortrait
                          ? mediaQuery.size.height * 0.1
                          : mediaQuery.size.width * 0.1,
                      backgroundImage: _image == null
                          ? AssetImage('assets/images/default_profile.jpg')
                          : FileImage(_image),
                      // child: _image == null
                      //     ? Image.asset('assets/images/default_profile.jpg')
                      //     : Image.file(_image),
                    ),
                  ),
                  Positioned(
                    top: 110,
                    left: 90,
                    child: MaterialButton(
                      onPressed: () {
                        _choseImageSource(context);
                      },
                      color: Theme.of(context).accentColor,
                      textColor: Colors.white,
                      child: Icon(
                        Icons.camera_alt,
                        size: 24,
                      ),
                      shape: CircleBorder(),
                    ),
                  ),
                  // IconButton(icon : Icon((Icons.camera_alt),size: 30,), onPressed: _getImage))
                ],
              ),
            ),
          ),
          _listTileBuilder(
            context,
            Icons.person,
            _userName,
            'This name will be visible to your neighbors',
            _editUserName,
          ),
          _listTileBuilder(
            context,
            Icons.phone_android,
            _phoneNumber,
            'This number will be register in database',
            _editPhoneNumber,
          ),
          _listTileBuilder(
            context,
            Icons.my_location,
            _address,
            'This address will be register in database and yo see post dependent on it',
            (context) async {
              final locationData = await Navigator.of(context).push<LatLng>(
                  MaterialPageRoute(builder: (ctx) => EditLocationScreen()));
              if (locationData != null) {
                if (!mounted) {
                  return;
                }
                setState(() {
                  _latitude = locationData.latitude;
                  _longtude = locationData.longitude;
                });
              }
            },
          ),
          // ListTile(
          //   leading: Padding(
          //       padding: EdgeInsets.only(bottom: 30),
          //       child: Icon(
          //         Icons.person,
          //         size: 32,
          //       )),
          //   title: Text(_userName),
          //   subtitle: Text(
          //     'This name will be visible to your neighbors',
          //     style: TextStyle(fontSize: 12),
          //   ),
          //   trailing: Icon(
          //     Icons.edit,
          //     size: 25,
          //   ),
          //   onTap: () => _editUserName(context),
          // ),
        ],
      ),
    );
  }
}
