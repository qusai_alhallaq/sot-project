import 'package:flutter/material.dart';

class TermsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Terms'),
        ),
        body: Center(
          child:
              Text('This is Terms', style: Theme.of(context).textTheme.title),
        ));
  }
}
